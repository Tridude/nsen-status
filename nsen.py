
## nsen.py
## Gets the currently playing video on Nsen (Vocaloid Channel on default) and posts it on twitter
## Program needs to be ran all the time.
## Scheduling is still being worked on. To do - Integrate crontab so program can exit when Nsen is on break.
## if duration is more than 10 minutes, reduce to 10 minutes

import os
import sys
import time
import datetime
import argparse

import logging, logging.handlers
import apscheduler.scheduler

import yaml
try:
   from yaml import CLoader as YamlLoader, CDumper as YamlDumper
except ImportError:
   # default to python libyaml
   from yaml import Loader as YamlLoader, Dumper as YamlDumper

import requests
import twitter
from bs4 import BeautifulSoup

class Nsen(object):

    # special html/xml characters
    special_characters = {
        #'&quot;':'"',
        '&gt;':'>',
        '&amp;amp;':'&',
       '&lt;':'<',
       '&#039;': "'",
    }

    def __init__(self, config, logging_level):
        self.config = config
        self.logger = logging.getLogger("apscheduler.scheduler")
        self.logger.setLevel(logging_level)
        handler = logging.handlers.RotatingFileHandler("nsen.log", maxBytes=100000, backupCount=2)
        handler.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
        self.logger.addHandler(handler)
        self.logger.info("Start")
        self.program_start_time = int(time.time())
        self.max_retry = 5
        self.done = False
        self.retry_count = 0
        self.channel_start_time = None
        self.twitter_api = None
        self.session = requests.Session()


    def initialize_twitter(self):
        try:
            self.twitter_api = twitter.Api(
               consumer_key = self.config["tConsumerKey"],
               consumer_secret = self.config["tConsumerSecret"],
               access_token_key = self.config["tAccessTokenKey"],
               access_token_secret = self.config["tAccessTokenSecret"]
            )
            return 0
        except Exception as e:
            self.logger.error("Failed to initialize twitter api:\t\n%s"%e)
            return 3

    def main_loop(self):
        # login
        self.nico_login()
        # Initialize Scheduler
        self.sched = apscheduler.scheduler.Scheduler(misfire_grace_time=10)
        self.sched.add_listener(self.schedule_listener, apscheduler.scheduler.EVENT_JOB_ERROR | apscheduler.scheduler.EVENT_JOB_MISSED)
        self.sched.start()
        self.retry_count = 0
        # First run will be in 3 seconds.
        self.sched.add_date_job(self.nico, datetime.datetime.now()+datetime.timedelta(0,3))

        while not self.done:
            time.sleep(3)
        return 0

    def schedule_listener(self, event):
        self.logger.error("%s"%event)
        self.retry_count += 1
        if self.retry_count >= self.max_retry:
            self.done = True
        else:
            self.sched.add_date_job(self.nico, datetime.datetime.now()+datetime.timedelta(0,4*self.retry_count))

    def nico_login(self):
        # Variables for error handling.
        self.done = False
        self.retry_count = 0
        # Get ready to open the login page
        # Exits when succeeded or too many errors
        while not self.done and self.retry_count < self.max_retry:
            try:
                # Login
                response = self.session.post("https://secure.nicovideo.jp/secure/login.php",
                    data={"mail" : self.config["username"], "password" : self.config["password"]})
                # Successfully loaded login page
                self.done = True
            except (requests.HTTPError, requests.RequestException) as e:
              self.logger.error("%s:\n\t%s"%(type(e), e))
              self.done = False
              self.retry_count += 1
              time.sleep(self.retry_count*4)
        # Cannot get login page. Exit program
        if self.retry_count >= self.max_retry:
            self.logger.error("Too many retries logging in.")
            self.done = True
        else:
            self.done = False

    def nico(self):
        try:
            # request channel status
            response = self.session.get("http://live.nicovideo.jp/api/getplayerstatus/nsen/%s"%self.config["channel"])
        except (requests.HTTPError, requests.RequestException) as e:
            self.logger.error("%s:\n\t%s"%(type(e), e))
            self.retry_count += 1
            if self.retry_count >= self.max_retry:
                self.done = True
            else:
                self.sched.add_date_job(self.nico, datetime.datetime.now()+datetime.timedelta(0 ,4*self.retry_count))
            return 1
        # soup the content
        soup = BeautifulSoup(response.content, "xml")
        # Get stream node of xml
        stream = soup.find("stream")
        # Get the title of the channel
        channel_title = stream.find("title").text
        self.channel_start_time = int(stream.find("start_time").text)
        #chEndTime = int(stream.find('end_time').text)
        time_now = int(time.time())
        # See if there's anything playing
        if len(stream.find("contents_list")) == 0:
            # Check if current time is past break time. 84600 is 23.5 hours.
            if time_now > self.channel_start_time+84600 and time_now < self.channel_start_time+86400:
                self.logger.info("Channel on break")
                #print "Channel on break. Exiting program."
                self.done = True
                return 0
            # Check again in 3 seconds. Channel is probably still changing songs
            self.sched.add_date_job(self.nico, datetime.datetime.now()+datetime.timedelta(0,3))
            return 0
        # Get the content node which includes title of the video and ID number
        content = stream.find("contents_list").find("contents")
        video_id = content.text[6:]
        if video_id == self.config["lastID"]:
            # Same video ID as last time. Probably still changing songs. Check again in 3 seconds.
            self.logger.info("Same video %s"%video_id)
            #print "Same video"
            self.sched.add_date_job(self.nico, datetime.datetime.now()+datetime.timedelta(0,3))
            return 0
        title = content.get("title")
        # Replace special characters
        for key,value in self.special_characters.items():
            if key in title:
                title = title.replace(key, value)
        # Set up message to tweet
        try:
            twitter_message = self.config["message"]%locals()
        except Exception as e:
            self.logger.warning("Bad message string in config: %s"%e)
            twitter_message =  + "%s Now On Air - %s #%s"%(channel_title, title, video_id)
        # tweet the message
        try:
            status = self.twitter_api.PostUpdate(twitter_message)
        except Exception, e:
            self.logger.exception("Error tweeting message: %s\n\t%s"%(twitter_message, e))
        # print twitter_message
        # book keeping
        self.config["lastID"] = video_id
        self.logger.info(twitter_message)
        # self.logger.info("%s %s"%(video_id, title))
        start_time = int(content.get("start_time"))
        duration = int(content.get("duration"))
        # Channel only plays the first 10 minutes.
        if duration > 600:
            duration = 600
        self.sched.add_date_job(self.nico, datetime.datetime.fromtimestamp(start_time+duration+2).strftime('%Y-%m-%d %H:%M:%S'))

if __name__ == "__main__":
    nsen_twitter_version = 1.1

    logging_levels = {
        0 : logging.NOTSET,
        1 : logging.DEBUG,
        2 : logging.INFO,
        3 : logging.WARNING,
        4 : logging.ERROR,
        5 : logging.CRITICAL,
    }

    script_dir = os.path.dirname(os.path.realpath(sys.argv[0]))

    arg_parser = argparse.ArgumentParser(
       description="Script for tweeting currently playing video on Nsen. Continuously runs until break.")
    arg_parser.add_argument("-v", "--version", action="store_true",
       help="Prints out script version.")
    arg_parser.add_argument("-f", "--config-file", metavar="CONFIG_FILE", type=str,
       default="%s/nsen_config.yaml"%script_dir,
       help="Path to configuration yaml file. Use full path if not running from script dir. Defaults to script_dir/nsen_config.yaml")
    arg_parser.add_argument("-L", "--log-level", metavar="LOG_LEVEL", type=int,
       choices=[0, 1, 2, 3, 4, 5], default=3,
       help="Level for the error log. Corresponds to the logging levels of Python logging module. 0-5.")

    args = arg_parser.parse_args()

    if args.version:
       print "Nsen twitter version: %s"%nsen_twitter_version
       sys.exit(0)

    try:
        f1 = open(args.config_file, "r")
        config = yaml.load(f1, Loader=YamlLoader)
        f1.close()
    except:
        print "cannot open config file. Exiting"
        sys.exit(2)

    nsen_obj = Nsen(config, logging_levels.get(args.log_level))
    if nsen_obj.initialize_twitter():
        sys.exit(3)
    try:
        nsen_obj.main_loop()
    except Exception as e:
        print "Exception during main loop:\n%s"%e
    try:
        f1 = open(args.config_file, "w")
        yaml.dump(config, f1, Dumper=YamlDumper, default_flow_style=False)
        f1.close()
    except Exception as e:
        print "Can't dump to yaml:\n\t%s"%e
        sys.exit(2)
